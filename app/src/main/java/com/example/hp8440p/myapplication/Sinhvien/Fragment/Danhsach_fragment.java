package com.example.hp8440p.myapplication.Sinhvien.Fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp8440p.myapplication.Sinhvien.Database;
import com.example.hp8440p.myapplication.Sinhvien.Model.MonHoc;
import com.example.hp8440p.myapplication.R;

import java.util.ArrayList;
import java.util.List;


public class Danhsach_fragment extends Fragment {

    Database db;
    Button btnDangKy;
    TextView tvNganh, tvChuyenNganh;
    ListView lvMonHoc;
    AutoCompleteTextView TvmonHoc;
    Spinner spnListNganh, spnChuyenNganh;
    public static String Nganh, ChuyenNganh, MonHoc;
    String TenMonHoc;
    public static List<String> listCheckMonHoc = new ArrayList<>();


    //----------------Ngành------//
    public static String[] nganhHocList = {"CÔNG NGHỆ THÔNG TIN", "KINH TẾ-ĐỐI NGOẠI", "NHÀ HÀNG - KHÁCH SẠN"};
    //-----------------Chuyên ngành---------------------//
    String[] nganhCNTTList = {"Ứng dụng phần mềm", "Lập trình Android", "Lập trình web", "Thiết kế đồ họa"};
    String[] nganhKTList = {"Digital Marketing Online", "PR & Tổ chức sự kiện"};
    String[] nganhQuanLyList = {"QT NHà Hàng-Khách sạn", "Hướng dẫn viên du lịch"};
    //-----------Môn Học-----------------//
    //---------------CNTT---------//
    String[] UdpmList = {" Photoshop", "Tiếng Anh", "Cơ bản về HTML/ CSS"};
    String[] ltAList = {"Cơ sở dữ liệu", "Java 1", "Java 2"};
    String[] ltWList = {"Kiến thức học tập cơ sở", "Thiết kế hình ảnh với Photoshop", "Kiến thức cơ bản về HTML/CSS/IS"};
    String[] tkDHList = {"Kỹ năng học tập", "Tin học cơ sở", "Tin học văn phòng", "Thiết kế hình ảnh với Photoshop"};
    //---------------KT----------//
    String[] DMOList = {"Kỹ năng học tập", "Marketing căn bản", "Tin học văn phòng", "SEO & Marketing trên công cụ tìm kiếm"};
    String[] PrList = {"Kỹ năng học tập", "Marketing căn bản", "Nhập môn Quan hệ", "Kỹ năng phỏng vấn và trả lời phỏng vấn"};
    String[] SalesList = {"Marketing căn bản", "Kiến thức về Nghiên cứu Marketing và Thực hành Quản trị bán hàng"};
    //----------------QuanLy--------//
    String[] QtksList = {"Kỹ năng học tập", "Tổng quan Du lịch – Nhà hàng – Khách sạn", "Kỹ năng giao tiếp"};
    String[] QtnhList = {"Kỹ năng học tập", "Tổng quan Du lịch – Khách sạn – Nhà hàng", "Tin học văn phòng"};
    String[] QtdlList = {"Kỹ năng học tập", "Tâm lý và kỹ năng giao tiếp, ứng xử với du khách"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_monhoc, container, false);
        AnhXa(view);
        ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, nganhHocList);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spnListNganh.setAdapter(adapter);

        spnListNganh.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    AdapterSpn(nganhCNTTList, spnChuyenNganh);
                    AddMonHoc(UdpmList, ltAList, ltWList, tkDHList);
                }
                if (i == 1) {
                    AdapterSpn(nganhKTList, spnChuyenNganh);
                    AddMonHoc(DMOList, PrList, SalesList, null);
                }
                if (i == 2) {
                    AdapterSpn(nganhQuanLyList, spnChuyenNganh);
                    AddMonHoc(QtksList, QtnhList, QtdlList, null);
                }
                AddData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btnDangKy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddData();
            }
        });
        return view;
    }

    //------------------FucTion-------------------//

    public void AddMonHoc(final String[] list1, final String[] list2, final String[] list3, final String[] list4) {
        spnChuyenNganh.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    Adapter(list1, lvMonHoc);
                    AddMonHocChiTiet(list1);
                }
                if (i == 1) {
                    Adapter(list2, lvMonHoc);
                    AddMonHocChiTiet(list2);
                }
                if (i == 2) {
                    Adapter(list3, lvMonHoc);
                    AddMonHocChiTiet(list3);
                }
                if (i == 3) {
                    Adapter(list4, lvMonHoc);
                    AddMonHocChiTiet(list4);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void AddMonHocChiTiet(String[] list) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, list);
        TvmonHoc.setAdapter(adapter);
    }

    public void AdapterSpn(String[] list, Spinner spinner) {
        ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spinner.setAdapter(adapter);
    }

    public void Adapter(String[] list, ListView lv) {
        ArrayAdapter<String> adapterMonHoc = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, list);
        adapterMonHoc.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        lv.setAdapter(adapterMonHoc);
    }

    public void AddData() {
        db = new Database(getContext());
        Nganh = (String) spnListNganh.getSelectedItem();
        ChuyenNganh = (String) spnChuyenNganh.getSelectedItem();
        MonHoc = TvmonHoc.getText().toString();
        tvNganh.setText(Nganh);
        tvChuyenNganh.setText(ChuyenNganh);
        if (MonHoc.equals("")) {
            Toast.makeText(getActivity(), "Không bỏ trống Môn học", Toast.LENGTH_SHORT).show();
        } else if (listCheckMonHoc.contains(MonHoc)==true) {
            Toast.makeText(getContext(), "Bạn đã đăng ký môn này nên không cần đăng ký lại.", Toast.LENGTH_LONG).show();
        } else {
            MonHoc monHoc = new MonHoc(Nganh, ChuyenNganh, MonHoc,null);
             if (db.AddMonHoc(monHoc) > 0) {
                 CheckMonHoc();
                Toast.makeText(getActivity(), ">Bạn đã đăng ký thành công môn học" + "\n" + ">Thông tin chi tiết :" + "\n" + ">" + Nganh + "\n" + ">" + ChuyenNganh + "\n" + ">" + MonHoc, Toast.LENGTH_LONG).show();
            }
        }
    }
    public void CheckMonHoc(){
        db = new Database(getActivity());
        Cursor cursor = db.getData("SELECT TenMonHoc FROM MonHoc");
        if (cursor!=null && cursor.getCount()>0){
            if (cursor.moveToFirst()){
                do {
                    TenMonHoc = cursor.getString(0);
                    listCheckMonHoc.add(TenMonHoc);
                }while (cursor.moveToNext());
            }
        }
        cursor.close();
    }

    public void AnhXa(View view) {
        spnListNganh = view.findViewById(R.id.SpnNganhHoc);
        spnChuyenNganh = view.findViewById(R.id.SpnChuyenNganh);
        lvMonHoc = view.findViewById(R.id.lvMonHoc);
        tvNganh = view.findViewById(R.id.tvNganh);
        tvChuyenNganh = view.findViewById(R.id.tvChuyenNganh);
        TvmonHoc = view.findViewById(R.id.tvMonHoc);
        btnDangKy = view.findViewById(R.id.btnDangKy);
    }
}
