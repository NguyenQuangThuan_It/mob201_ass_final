package com.example.hp8440p.myapplication.Sinhvien.Model;

public class MonHoc {

    String Nganh,ChuyenNganh,TenMonHoc,IDAuto;


    public MonHoc() {
    }

    public String getNganh() {

        return Nganh;
    }

    public void setNganh(String nganh) {
        Nganh = nganh;
    }

    public String getChuyenNganh() {
        return ChuyenNganh;
    }

    public void setChuyenNganh(String chuyenNganh) {
        ChuyenNganh = chuyenNganh;
    }

    public String getTenMonHoc() {
        return TenMonHoc;
    }

    public void setTenMonHoc(String tenMonHoc) {
        TenMonHoc = tenMonHoc;
    }

    public String getIDAuto() {
        return IDAuto;
    }

    public void setIDAuto(String IDAuto) {
        this.IDAuto = IDAuto;
    }


    public MonHoc(String nganh, String chuyenNganh, String tenMonHoc, String IDAuto) {

        Nganh = nganh;
        ChuyenNganh = chuyenNganh;
        TenMonHoc = tenMonHoc;
        this.IDAuto = IDAuto;
    }
}
