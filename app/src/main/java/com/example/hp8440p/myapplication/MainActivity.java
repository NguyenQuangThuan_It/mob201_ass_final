package com.example.hp8440p.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.hp8440p.myapplication.Facebook.facebook;
import com.example.hp8440p.myapplication.Sinhvien.Sinhvien_activity;
import com.example.hp8440p.myapplication.Map.MapsActivity;
import com.example.hp8440p.myapplication.News.NewsActivity;


public class MainActivity extends AppCompatActivity {
ImageView map,news,course,face;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        anhsa();
        onClick();


    }

    private void onClick() {


        anhsa();



        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MapsActivity.class);
                startActivity(intent);
            }
        });



        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewsActivity.class);
                startActivity(intent);
            }
        });



        course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Sinhvien_activity.class);
                startActivity(intent);
            }
        });



        face.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, facebook.class);
                startActivity(intent);
            }
        });


    }

    private void anhsa() {

        map = findViewById(R.id.map);
        news = findViewById(R.id.News);
        course = findViewById(R.id.course);
        face = findViewById(R.id.facebook);
    }
}
